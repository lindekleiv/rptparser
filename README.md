A simple Python parser for the .RPT file format. I wrote this to convert a database dump from Microsoft SQL Server to MySQL and Django. Tested on 15 tables of various sizes, and up to 15 000 rows.

Created by Olav Andreas Lindekleiv.
http://lindekleiv.com/

Released under the MIT License.
